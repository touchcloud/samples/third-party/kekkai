module KekkaiDemo

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/line/line-bot-sdk-go v7.1.0+incompatible
	go.uber.org/zap v1.13.0
	gocv.io/x/gocv v0.22.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
