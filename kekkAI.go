package main

type kekkAI struct {
	DeviceId string `json:"deviceId"`
	Token    string `json:"token"`
	Type     string `json:"type"`
	Data     moment `json:"data"`
}

type moment struct {
	Version  string `json:"Version"`
	Location string `json:"Location"`
	CamID    string `json:"CamID"`
	CamName  string `json:"CamName"`
	Time     string `json:"Time"`
	Item     string `json:"Item"`
	ItemID   string `json:"ItemID"`
	Label    string `json:"Label"`
	Count    int    `json:"Count"`
	TimeZone string `json:"TimeZone"`
	Uuid     string `json:"uuid"`
	Snapshot string `json:"snapshot"`
}
