### Dockerfile
# Use an official Debian as a parent image
FROM debian:latest

# Set the working directory to /app
WORKDIR /app

# Copy all data in current Directory to image
COPY . /app/KekkAIDemo

RUN apt-get -y update \
    && apt-get -y install ca-certificates

# Make port 8081 available to the world outside this container
EXPOSE 8081

# Run app.py when the container launches
ENTRYPOINT ["/app/KekkAIDemo/KekkAIDemo"]
CMD ["/app/KekkAIDemo/KekkAIDemo"]
