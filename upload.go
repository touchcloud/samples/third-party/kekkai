package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

type imgurData struct {
	Link string `json:"link"`
}

type imgurRes struct {
	Data    imgurData `json:"data"`
	Success bool      `json:"success"`
	Status  int       `json:"status"`
}

var accessToken = "7ce038188f1052ae3b31bdd89c105d86739b230a"

func upload(image io.Reader, token string) string {
	var buf = new(bytes.Buffer)
	writer := multipart.NewWriter(buf)

	part, _ := writer.CreateFormFile("image", "dont care about name")
	io.Copy(part, image)

	writer.Close()
	req, _ := http.NewRequest("POST", "https://api.imgur.com/3/image", buf)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Authorization", "Bearer "+token)

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		sugarLogger.Error("Failed to read: ", err)
	}
	defer res.Body.Close()
	b, _ := ioutil.ReadAll(res.Body)

	var imgures imgurRes
	err = json.Unmarshal(b, &imgures)
	sugarLogger.Info(imgures.Data.Link)
	return imgures.Data.Link
}
