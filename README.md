# Outline
- ## [Description](#description-1)

- ## [Installation](#installation-1)

- ## [Usage](#usage-1)

- ## [KekkAI server setup](#KekkAI-server-setup-1)

- ## [FAQ](#faq-1)

- ## [If you want to use email notification](#if-you-want-to-use-email-notification-1)

- ## [If you want to use Line notification](#if-you-want-to-use-line-notification-there-are-two-parts-to-complete-first)

# Description:
KekkAIDemo is Touchcloud's KekkAI endpoints.
It can automatically notify KekkAI moment newest information to you by Email & Line.


# Installation:
```
git clone https://gitlab.com/touchcloud/samples/third-party/kekkai.git

If you have set SSH key, you can use
git clone git@gitlab.com:touchcloud/samples/third-party/kekkai.git
```
# Usage:
```
- cd kekkai
```
- If you only want to use email method
```
- ./KekkAIDemo -emailAccount user@gmail.com -emailPassword password -email receiver1@gmail.com -email receiver2@yahoo.com.tw 
```
- If you only want to use line method
```
- ./KekkAIDemo -lineWebhook https://6a5e838b.ngrok.io/callback -line U2ddbded68f4ebb8edd4412c4d4a7e8c7
```

- If  you want to use both methods
```
- ./KekkAIDemo -lineWebhook -line -emailAccount -emailPassword -email
```
* `-emailAccount`: Your Gmail Account
* `-emailPassowrd`: Your Gmail Password (If your password have specific letter like "!@#$%^&*", Please use ' ' quote your password eg: -emailPassword '!password' )
* `-email`: Email Receiver who you want to send mail

* `-lineWebhook`: Your LineBot Webhook
* `-line`: KekkAIDemo linebot offer's LineID

## KekkAI server setup:

1. You have to go to http://localhost/ , and enter to Sytem Config
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/KekkAIServer1.png)

2. At KekkAI option, type http://localhost:8081/sender to Url and click Add button
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/KekkAIServer2.png)

3. Finally enable KekkAI,setup and open KekkAI's endpoint to check the data
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/KekkAIServer3.png)

## FAQ:
1. If you encounter any unknowed problem, you can check the KekkAIDemo.log by use below command, and check the error code
```
cd kekkai
cat KekkAIDemo.log
```
2. If you already setup all thing but the email and line doesn't work, you can check log, whether it show "certificate signed by unknown authority"

   If it is, it because your computer doesn't have certification, try to use below command,and retry the endpoint
```
apt-get update
apt-get install ca-certificates
```

# Notice:
## 如果你想要使用Email通知功能
1. 首先你必須登入發信之Gmail帳號

2. 點選右上個人圖像,管理你的google帳戶 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail1.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail2.png)

3. 點選左方側欄安全性之選項 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail3.png)

### * 如果你未開啟兩步驟驗證功能
4. 低安全性應用程式存取權 設置為開啟 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail4.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail5.png)

### * 如果你已開啟兩步驟驗證功能
4. 點選兩步驟驗證下方之應用程式密碼 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail6.png)

5. 選擇其他應用程式,新增一組應用程式密碼,名稱可自取 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail7.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail8.png)

6. 記下密碼以供日後KekkAIDemo程式使用 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail9.png)

**Notice:**日後使用KekkAIDemo程式之email發送功能時,-emailPassword之欄位請使用此十六位應用程式密碼
```
eg: ./KekkAIDemo -emailAccount blue14753@gmail.com -emailPassword sjqsmkzqkwlrrsqt -email receiver@gmail.com
```

## If you want to use email notification
1. first you have to go to gmail and login sender mail 

2. Click on the top right personal image to manage your google account
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail10.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail11.png)

3. Click the security option on the left sidebar 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail12.png)

### * If you have not enable 2-step verification 
4. Less secure app access Set to On 
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail13.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail14.png)
### * If you have enable 2-step verification 
4. Click on the application password below 2-step verification
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail15.png)

5. Choose type another, add a set of application passwords, the name can be self-named
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail16.png)

![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail17.png)

6. Write down the password for future use by KekkAIDemo
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/gmail18.png)

**Notice:** When using the email function of KekkAIDemo program in the future, please use this 16-digit application password in the -emailPassword field
```
eg: ./KekkAIDemo -emailAccount blue14753@gmail.com -emailPassword sjqsmkzqkwlrrsqt -email receiver@gmail.com
```

## 如果你想要使用Line通知功能,有兩塊步驟需先完成

### LineBot Server
#### linebot Part:
- 請查看[LineBot](./linebot)子專案資料夾內容


### LineBot Client
1. 在註冊完且確認LineBot可正常運作後,加入LineBot機器人好友,或將LineBot加入至群組內

2. 在對話欄任意發送一句話,LineBot將回覆屬於你或群組之 KekkAIDemo LineID
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/KekkAILineBot1.png)

3. 在執行KekkAIDemo之程式時後綴加上 -line LineID 即可發送通知至該ID

## If you want to use Line notification, there are two parts to complete first

### LineBot Server
#### linebot Part:
- Please look [Linebot](./linebot) subProject directory

### LineBot Client
1. After registering and confirming that LineBot can work normally, join a LineBot robot friend, or add LineBot to a group

2. Send any sentence in the Chat box, LineBot will reply to the KekkAIDemo LineID belonging to you or the group
![image](https://gitlab.com/touchcloud/samples/third-party/kekkai/-/raw/develop/tutorial/KekkAILineBot1.png)

3. When running KekkAIDemo's program, add the -line LineID suffix that you can send a notification to that ID



